#include "gtest/gtest.h"
#include "pch.h"
#include "../Server/CardDeck.h"

TEST(Card, Create) {
	Card* TestCard = new Card(3, 7);
	ASSERT_EQ(3, TestCard->GetType());
	ASSERT_EQ(7, TestCard->GetValue());
	TestCard = new Card(1, 3);
	ASSERT_EQ(1, TestCard->GetType());
	ASSERT_EQ(3, TestCard->GetValue());
}

TEST(Card, CardPoints) {
	Card* TestCard = new Card(3, 7);
	TestCard->SetPoints(30);
	ASSERT_EQ(30, TestCard->GetPoints());
	TestCard = new Card(1, 3);
	TestCard->SetPoints(4);
	ASSERT_EQ(4, TestCard->GetPoints());
}


TEST(Deck, CreateDeck) {
	Deck* TestDeck = new Deck(true);
	ASSERT_EQ(0, TestDeck->GetDeck()->size());
	TestDeck = new Deck(false);
	ASSERT_EQ(52, TestDeck->GetDeck()->size());
	int k = 0;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 13; j++)
		{
			ASSERT_EQ(i, TestDeck->GetDeck()->at(k)->GetType());
			ASSERT_EQ(j, TestDeck->GetDeck()->at(k)->GetValue());
			k++;
		}
	}
}

TEST(Deck, AddRemoveLastCard) {
	Deck* TestDeck = new Deck(true);
	Card* FirstCard = new Card(2, 7);
	TestDeck->AddCard(FirstCard);
	ASSERT_EQ(1, TestDeck->GetDeck()->size());
	Card* BackCard = TestDeck->PopLastCard();
	ASSERT_EQ(2, BackCard->GetType());
}

TEST(Deck, Shuffling ) //conajmniej po�owa kart musi by� w innych miejscach)
{ 
	Deck* FirstDeck = new Deck(false);
	Deck* SecondDeck = new Deck(false);

	for (int j = 0; j < 20; j++)
	{
		delete SecondDeck;
		SecondDeck = new Deck(false);
		FirstDeck->ShufflingCards();
		int diferences = 0;
		for (int i = 0; i < FirstDeck->GetDeck()->size(); i++)
		{
			if (FirstDeck->GetDeck()->at(i)->GetType() != SecondDeck->GetDeck()->at(i)->GetType() || FirstDeck->GetDeck()->at(i)->GetValue() != SecondDeck->GetDeck()->at(i)->GetValue())
			{
				diferences++;
			}
		}
		ASSERT_LT((FirstDeck->GetDeck()->size() / 2), diferences);
	}

	Deck* EmptyDeck = new Deck(true);
	ASSERT_NO_THROW(EmptyDeck->ShufflingCards());
}