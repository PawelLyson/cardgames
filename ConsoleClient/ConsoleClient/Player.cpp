#include "stdafx.h"
#include <SFML/Network.hpp>
#include <iostream>
#include <string>
#include <thread>
#include "Globals.h"
#include "Player.h"
#include "ServerListOperations.h"
#include "LobbyGameOperations.h"
#include "BlackJackOperations.h"

extern int GlobalClientID;

struct DataPacketTCP
{
	sf::Int16 ClientID;
	sf::Int16 OperationID;
	std::string Data;
};
sf::Packet& operator <<(sf::Packet& packet, const DataPacketTCP& m)
{
	return packet << m.ClientID << m.OperationID << m.Data;
}
sf::Packet& operator >>(sf::Packet& packet, DataPacketTCP& m)
{
	return packet >> m.ClientID >> m.OperationID >> m.Data;
}

Player::Player()
{
	IP = sf::IpAddress("127.0.0.1");
	Port = 53000;
	PrepareOperations();
}


Player::~Player()
{
}

void Player::Connect()
{
	if (ConnectionToServer.connect(IP, Port) != sf::Socket::Done) // je�li funkcja connect zwr�ci sf::Socket::Done oznacza to, �e wszystko posz�o dobrze
	{
		std::cout << "Nie mozna polaczyc sie z " << IP.toString() << std::endl;
		getchar();
		exit(0);
	}
	else
	{
		WaitForCommandThread = new std::thread(&Player::ReceiveMsg,this);
		std::cout << "Polaczono " << IP.toString() << std::endl;
		RegisterPlayer();
	}
	WaitForCommandThread->join();
}

void Player::SendMsg(int operationType, std::string tekst)
{
	sf::Packet ToSendPacket;
	DataPacketTCP DataStruct;

	DataStruct = { sf::Int16(ClientID),sf::Int16(operationType),tekst };
	ToSendPacket << DataStruct;
	ConnectionToServer.send(ToSendPacket);
	ToSendPacket.clear();
}

void Player::UserInput() // nie u�ywane
{
	std::string Text;
	int OperationType;
	while (true)
	{
		printf("Podaj typ opracji:\n");
		OperationType = GetIntFromRange(0,100);
		printf("Podaj tresc:\n");
		Text = GetString();
		SendMsg(OperationType, Text);
	}

	getchar();
}

void Player::ReceiveMsg()
{
	sf::Packet ReturnedPacket;
	DataPacketTCP DataPacket;
	// TCP socket:
	bool isWaitingForCommand = true;
	while (isWaitingForCommand)
	{
		if (ConnectionToServer.receive(ReturnedPacket) == sf::Socket::Done)
		{
			ReturnedPacket >> DataPacket;
			RerouteCommands(DataPacket.ClientID, DataPacket.OperationID, DataPacket.Data);
			//printf("Client: %d\n Operation: %d\nData: %s\n", DataPacket.ClientID, DataPacket.OperationID, DataPacket.Data.c_str());
			ReturnedPacket.clear();
		}
		else
		{
			printf("Data received error\n");
			break;
		}
	}
}

void Player::RerouteCommands(int targetID, int operationType, std::string data)
{
	//printf("===========\n");
	//printf("Otrzymane DaneL\n");
	//printf("%d\n", targetID);
	//printf("%d\n",operationType);
	//printf("%s\n",data.c_str());
	//printf("===========\n");
	if (operationType == 10000)
	{
		int ToSetStatus = std::stoi(data);
		if (ToSetStatus == 1)
		{
			PlayerStatus = 1;
			ServerListOp.GetWelcome();
		}
		else if (ToSetStatus == 2)
		{
			PlayerStatus = 2;
			SendMsg(20, std::string());
		}
		else if (ToSetStatus == 3)
		{
			PlayerStatus = 3;
		}
	}

	else if (PlayerStatus == 0) // registration
	{
		if (operationType == 1)
		{
			PlayerStatus = 1;
		}
		else RegisterPlayer();
	}
	else if (PlayerStatus == 1) //in server list
	{
		PlayerStatus = ServerListOp.RerouteCommands(targetID, operationType, data);
	}
	else if (PlayerStatus == 2) //in game lobby
	{
		PlayerStatus = LobbyGameOp.RerouteCommands(targetID, operationType, data);
	}
	else if (PlayerStatus == 3) //in game lobby
	{
		PlayerStatus = BlackJackOp.RerouteCommands(targetID, operationType, data);
	}
}
void Player::RegisterPlayer()
{
	printf("Podaj ID clienta:\n");
	GlobalClientID = GetIntFromRange(1, 32000);
	printf("Podaj nazwe gracza:\n");
	std::string Name = GetString(true, 20, 4);
	SendMsg(1, std::to_string(GlobalClientID));
	SendMsg(10, Name);
}

void Player::PrepareOperations()
{
	using namespace std::placeholders;
	std::function<void(int, std::string)> CallbackSendMsg = std::bind(&Player::SendMsg, this, _1,_2);
	ServerListOp = ServerListOperations(CallbackSendMsg);
	LobbyGameOp = LobbyGameOperations(CallbackSendMsg);
	BlackJackOp = BlackJackOperations(CallbackSendMsg);
}