#pragma once
#include <string>
#include <functional>


class LobbyGameOperations
{
private:
	std::function<void(int, std::string)> CallbackSendMsg;
	void PrintCommands();
	void InputSelectAction();
public:
	LobbyGameOperations();
	LobbyGameOperations(std::function<void(int, std::string)> callbackSendMsg);
	int RerouteCommands(int targetID, int operationType, std::string data);
	~LobbyGameOperations();
};

