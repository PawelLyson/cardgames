#include "stdafx.h"
#include <string>
#include <functional>
#include <iostream>
#include "LobbyGameOperations.h"
#include "Globals.h"



LobbyGameOperations::LobbyGameOperations()
{
}

LobbyGameOperations::~LobbyGameOperations()
{
}

LobbyGameOperations::LobbyGameOperations(std::function<void(int, std::string)> callbackSendMsg)
{
	CallbackSendMsg = callbackSendMsg;
}

int LobbyGameOperations::RerouteCommands(int targetID, int operationType, std::string data)
{
	if (operationType == 20)
	{
		system("cls");
		printf("================================\n");
		printf("%s\n", data.c_str());
		PrintCommands();
		InputSelectAction();
	}
	else if (operationType == 22)
	{
		//printf("Polaczono z Gra\n");
		CallbackSendMsg(25, std::string(""));
		return 3;
	}
	return 2;
}

void LobbyGameOperations::PrintCommands()
{
	printf("================================\n");
	printf(" 1 - Start game/Ready\n");
	printf(" # - Change name\n");
	printf(" # - Change game\n");
	printf(" # - Leave Lobby\n");
}

void LobbyGameOperations::InputSelectAction()
{
	printf("Podaj kod operacji:\n");
	int OperationCode = GetIntFromRange(1,4);
	std::string OperationData;

	if (OperationCode == 1)
	{
		CallbackSendMsg(22, std::string(""));
	}
	else if (OperationCode == 2)
	{
		printf("Podaj nowa nazwa lobby:\n");
		OperationData = GetString();
		//CallbackSendMsg(21, OperationData);
	}
	else if (OperationCode == 3)
	{
		printf("Zmien gre:\n");
		OperationData = GetString();
		//CallbackSendMsg(21, OperationData);
	}
	else if (OperationCode == 4)
	{
		//CallbackSendMsg(24, std::string(""));
	}
	printf("Oczekiwanie na pozostalych graczy");
}