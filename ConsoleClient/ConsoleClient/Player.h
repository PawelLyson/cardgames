#pragma once
#include <string>
#include <thread>
#include <SFML/Network.hpp>
#include "ServerListOperations.h"
#include "LobbyGameOperations.h"
#include "BlackJackOperations.h"

class Player
{
private:
	int PlayerStatus = 0;
	sf::IpAddress IP;
	unsigned int Port;
	sf::TcpSocket ConnectionToServer;
	std::thread* WaitForCommandThread;
	int ClientID = 0;
	ServerListOperations ServerListOp;
	LobbyGameOperations LobbyGameOp;
	BlackJackOperations BlackJackOp;
public:
	Player();
	void Connect();
	void SendMsg(int operationType, std::string tekst);
	void UserInput();
	void ReceiveMsg();
	void RegisterPlayer();
	void PrepareOperations();
	void RerouteCommands(int targetID, int operationType, std::string data);
	~Player();
};

