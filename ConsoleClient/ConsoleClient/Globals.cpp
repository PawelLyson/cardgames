#include "stdafx.h"
#include <sstream>
#include <iostream>
#include <algorithm>
#include <cctype>
#include "Globals.h"

int GlobalClientID = 0;
int Money = 0;
int MoneyOnTable = 0;

int GetIntFromRange(int min, int max)
{
	std::string OneLine;
	int Number;
	bool IsValid = true;
	while (true)
	{
		getline(std::cin, OneLine);
		try 
		{
			Number = std::stoi(OneLine);
			IsValid = true;
		}
		catch (std::invalid_argument e)
		{
			IsValid = false;
		}
		catch (std::out_of_range e)
		{
			IsValid = false;
		}

		if (IsValid && Number && Number >= min && Number <= max)
		{
			return Number;
		}
		else
		{
			printf("Blednie podane liczba z zakresu\n");
		}
	}
	return 0;
}

int GetIntFromRange(int number)
{
	return GetIntFromRange(number, number);
}

std::string GetString(bool removeStrange, int max, int min)
{
	std::string OneLine;
	while (true)
	{
		std::getline(std::cin, OneLine);
		if (removeStrange)
		{
			OneLine.erase(std::remove_if(OneLine.begin(), OneLine.end(), [](char c) { return !std::isalnum(c); }), OneLine.end());
		}

		if (OneLine.length() >= min && OneLine.length() <= max)
		{
			return OneLine;
		}
		else
		{
			printf("Blednie dlugosc tekstu\n");
		}
	}
	return std::string();
}