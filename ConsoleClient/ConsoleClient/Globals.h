#pragma once

int GetIntFromRange(int min, int max);
int GetIntFromRange(int min);
std::string GetString(bool removeStrange = true, int max = 20, int min = 1);