#include "stdafx.h"
#include <string>
#include <iostream>
#include <functional>
#include "BlackJackOperations.h"
#include "Globals.h"

extern int GlobalClientID;
extern int Money;
extern int MoneyOnTable;


BJPlayerInfo::BJPlayerInfo(int id, std::string name,int money)
{
	ClientID = id;
	Name = name;
	Money = money;
}

BlackJackOperations::BlackJackOperations()
{
}

BlackJackOperations::~BlackJackOperations()
{
}

BlackJackOperations::BlackJackOperations(std::function<void(int, std::string)> callbackSendMsg)
{
	CallbackSendMsg = callbackSendMsg;
}

int BlackJackOperations::RerouteCommands(int targetID, int operationType, std::string data)
{
	if (operationType == 100)
	{
			system("cls");
			PrintPlayersList();
			PrintPlayerInfo();
		if (targetID == GlobalClientID)
		{
			PrintMoveInfo();
			MakeMove();
		}
		else
		{
			printf("================================\n");
			printf("Czekaj na swoj ruch\n");
		}
	}
	else if (operationType == 105)
	{
		MakeContinue();
	}
	else if (operationType == 120)
	{
		MoneyOnTable = 50;
		//printf("Pierwsze rozdanie\n");
		//CallbackSendMsg(25, std::string(""));
	}
	else if (operationType == 130)
	{
		if (targetID == GlobalClientID)	CardsInHand.push_back(data);
	}
	else if (operationType == 135)
	{
		if (GlobalClientID != targetID)
		{
			if (targetID == -1) printf("Kroupier:\n");
			else printf("Gracz z ID %d:\n", targetID);
			printf("Dobiera Karte\n");
			SetPlayerLastAction(targetID, std::string("Dobral karte"));
			AddCardAmountToPlayer(targetID);
		}
	}
	else if (operationType == 136)
	{
		if (GlobalClientID != targetID)
		{
			if (targetID == -1) printf("Kroupier:\n");
			else printf("Gracz z ID %d:\n", targetID);
			printf("Dobiera Karte\n");
			SetPlayerLastAction(targetID, std::string("Dobral karte i podbil stawke"));
			DoublePlayerMoney(targetID);
			AddCardAmountToPlayer(targetID);
		}
	}
	else if (operationType == 137)
	{
		if (GlobalClientID != targetID)
		{
			if (targetID == -1) printf("Kruopier:\n");
			else printf("Gracz z ID %d:\n", targetID); 
			printf("Czeka\n");
			SetPlayerLastAction(targetID, std::string("Czeka"));
		}
	}
	else if (operationType == 151)
	{
		if (GlobalClientID != targetID)	printf("Gracz z ID %d Przegral\n", targetID);
		else printf("PRZEGRALES\n");
		CleanInfoList();
	}
	else if (operationType == 152)
	{
		if (GlobalClientID != targetID) printf("Gracz z ID %d Zremisowal\n", targetID);
		else printf("REMIS\n");
		CleanInfoList();
	}
	else if (operationType == 153)
	{
		if (GlobalClientID != targetID)	printf("Gracz z ID %d Wygral\n", targetID);
		else printf("WYGRALES\n");
		CleanInfoList();
	}
	else if (operationType == 155)
	{
		if (GlobalClientID == targetID)
		{
			CardsInHand.clear();
		}
	}
	else if (operationType == 170)
	{
		if (GlobalClientID != targetID)
		{
			int FirstSepator = data.find_first_of('|');
			int SecondSepator = data.find_first_of('|',FirstSepator+1);
			int ClientID = std::stoi(data.substr(0, FirstSepator));
			std::string ClientName = data.substr(FirstSepator + 1, SecondSepator - FirstSepator - 1);
			std::string test = data.substr(SecondSepator + 1, data.size() - SecondSepator);
			int Money = std::stoi(data.substr(SecondSepator + 1, data.size()-SecondSepator));
			PlayersInfoList.push_back(BJPlayerInfo(ClientID,ClientName,Money));
		}
		else
		{
			int LastSepator = data.find_last_of('|');
			Money = std::stoi(data.substr(LastSepator+1, data.size() - LastSepator));
		}
	}
	else
	{
		printf("----Wiadomosc dla %d\n", targetID);
		printf("----Operation: %d \n", operationType);
		printf("----data: %s \n", data.c_str());
	}
	return 3;
}

void BlackJackOperations::PrintMoveInfo()
{
	printf("================================\n");
	printf("Twoj ruch\n");
	printf("1 - Poczekaj\n");
	printf("2 - Dobierz karte\n");
	if (IsDoubleDown())printf("3 - Dobierz karte i podbij stawke\n");
}

void BlackJackOperations::PrintPlayerInfo()
{
	printf("================================\n");
	printf("Pieniadze: %d\n", Money);
	printf("Pieniadze na stole: %d\n", MoneyOnTable);

	for (unsigned int i = 0; i<CardsInHand.size(); i++)
	{
		printf("%s\n", CardsInHand.at(i).c_str());
	}
}

void BlackJackOperations::PrintPlayersList()
{
	printf("================================\n");
	for (unsigned int i = 0; i < PlayersInfoList.size(); i++)
	{
		if (PlayersInfoList.at(i).ClientID != -1)
		{
			printf("%s\n", PlayersInfoList.at(i).Name.c_str());
			printf("Pieniadze na stole: %d\n", PlayersInfoList.at(i).MoneyTable);
		}
		else
		{
			printf("Krupier\n");
			printf("%s\n", PlayersInfoList.at(i).Name.c_str());
		}
		printf("Karty w rece: %d\n", PlayersInfoList.at(i).CardsInHand);
		if (PlayersInfoList.at(i).LastAction != "") printf("Ostatnia akcja: %s\n", PlayersInfoList.at(i).LastAction.c_str());
		printf("\n");
	}
}

void BlackJackOperations::MakeMove()
{
	printf("================================\n");
	printf("Podaj kod Ruchu:\n");
	int MoveID;
	if (IsDoubleDown()) MoveID = GetIntFromRange(1, 3);
	else MoveID = GetIntFromRange(1, 2);

	if (MoveID == 1)
	{
		CallbackSendMsg(101, std::string("1"));
	}
	else if (MoveID == 2)
	{
		CallbackSendMsg(101, std::string("2"));
	}
	else if (MoveID == 3)
	{
		Money -= MoneyOnTable;
		MoneyOnTable *= 2;
		CallbackSendMsg(101, std::string("3"));
	}
}

void BlackJackOperations::MakeContinue()
{
	printf("================================\n");
	printf("Czy chcesz grac dalej ? t/n :\n");
	std::string Response = GetString(true, 1, 1);
	if (Response == "t" || Response == "T") CallbackSendMsg(105, std::string(""));
	else CallbackSendMsg(106, std::string(""));
	printf("Oczekiwanie na pozostalych graczy:\n");
}

void BlackJackOperations::SetPlayerLastAction(int playerID, std::string lastAction)
{
	for (unsigned int i = 0; i < PlayersInfoList.size(); i++)
	{
		if (PlayersInfoList.at(i).ClientID == playerID)
		{
			PlayersInfoList.at(i).LastAction = lastAction;
			return;
		}
	}
}

bool BlackJackOperations::IsDoubleDown()
{
	if ((Money - MoneyOnTable) < 0) return false;
	return true;
}

void BlackJackOperations::DoublePlayerMoney(int playerID)
{
	for (unsigned int i = 0; i < PlayersInfoList.size(); i++)
	{
		if (PlayersInfoList.at(i).ClientID == playerID)
		{
			PlayersInfoList.at(i).MoneyTable *=2;
			return;
		}
	}
}


void BlackJackOperations::AddCardAmountToPlayer(int playerID)
{
	for (unsigned int i = 0; i < PlayersInfoList.size(); i++)
	{
		if (PlayersInfoList.at(i).ClientID == playerID)
		{
			PlayersInfoList.at(i).CardsInHand += 1;
			return;
		}
	}
}

void BlackJackOperations::CleanInfoList()
{
	PlayersInfoList.clear();
}