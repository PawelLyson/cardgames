#include "stdafx.h"
#include <functional>
#include <iostream>
#include "ServerListOperations.h"
#include "Globals.h"

ServerListOperations::ServerListOperations()
{
}

ServerListOperations::ServerListOperations(std::function<void(int, std::string)> callbackSendMsg)
{
	CallbackSendMsg = callbackSendMsg;
}

ServerListOperations::~ServerListOperations()
{
}

int ServerListOperations::RerouteCommands(int targetID, int operationType, std::string data)
{
	if (operationType == 1)
	{
		CallbackSendMsg(2, std::string(""));
	}
	else if (operationType == 2)
	{
		system("cls");
		printf("Lista Serwerow: \n");
		printf("%s", data.c_str());
		PrintCommands();
		InputSelectAction();
	}
	else if (operationType == 3) // Zmiana stanu w kt�rym jest klient (na in Lobby) i zapytanie o dane serwera
	{
		//printf("Polaczono z lobby\n"); 
		CallbackSendMsg(20, std::string(""));
		return 2;
	}
	else if (operationType == 4)
	{
		printf("Stworzono serwer\n");
		CallbackSendMsg(20, std::string(""));
		PrintCommands();
		InputSelectAction();

	}
	return 1;
}

void ServerListOperations::GetWelcome()
{
	CallbackSendMsg(2, std::string(""));
}

void ServerListOperations::PrintCommands()
{
	printf("================================\n");
	printf(" 1 - Laczenie sie do serwera\n");
	printf(" 2 - Stworzenie nowego serwera\n");
}

void ServerListOperations::InputSelectAction()
{
	printf("Podaj kod operacji:\n");
	int OperationID = GetIntFromRange(1,2);
	std::string DataName;

	if (OperationID == 1)
	{
		printf("Podaj numer lobby:\n");
		DataName = GetString();
		CallbackSendMsg(3, DataName);
	}
	else
	{
		printf("Podaj nazwe gry:\n");
		DataName = GetString();
		CallbackSendMsg(4, DataName);
	}
}