#pragma once
#include <string>
#include <functional>
#include <vector>

class BJPlayerInfo
{
public:
	int ClientID;
	std::string Name;
	std::string LastAction = "";
	int Money = 0;
	int MoneyTable = 50;
	int CardsInHand = 2;
	BJPlayerInfo(int id, std::string name,int money);
};

class BlackJackOperations
{
private:
	std::function<void(int, std::string)> CallbackSendMsg; 
	std::vector<std::string> CardsInHand = std::vector<std::string>();
	std::vector<BJPlayerInfo> PlayersInfoList;
	void PrintMoveInfo();
	void PrintPlayerInfo();
	void PrintPlayersList();
	void MakeMove();
	void MakeContinue();
	void SetPlayerLastAction(int playerID, std::string lastAction);
	bool IsDoubleDown();
	void DoublePlayerMoney(int playerID);
	void AddCardAmountToPlayer(int playerID);
	void CleanInfoList();
public:
	BlackJackOperations();
	BlackJackOperations(std::function<void(int, std::string)> callbackSendMsg);
	int RerouteCommands(int targetID, int operationType, std::string data);
	~BlackJackOperations();
};

