#pragma once
#include <functional>
#include <string>

class ServerListOperations
{
private:
	std::function<void(int, std::string)> CallbackSendMsg;
public:
	ServerListOperations();
	ServerListOperations(std::function<void(int, std::string)> callbackSendMsg);
	int RerouteCommands(int targetID, int operationType, std::string data);
	void GetWelcome();
	void PrintCommands();
	void InputSelectAction();
	~ServerListOperations();
};

