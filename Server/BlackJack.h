#pragma once
#include "Players.h"
#include "SimpleGame.h"

namespace bjGame {

	class ComputerPlayer : public Player
	{
	public:
		ComputerPlayer(std::string name = "Komputer");
		int MakeMove() override;
		virtual std::string GetName() override;
		void SetReady(bool ready) override;
		bool IsReady() override;
	};

	class Croupier : public ComputerPlayer
	{
		public:
			Croupier();
			int MakeMove() override;
			std::string GetName() override;
	};
	class BlackJack: public SimpleGame
	{
		public:
			Croupier *GameCroupier;
			BlackJack();
			BlackJack(std::vector<Player*>* playersList);

			bool PlayerAction(Player* onePlayer,int move) override;
			bool PlayerTakeCard(Player* onePlayer);
			void CheckWinner() override;
			void PrepareGame() override;
			static int ComputePointsInHand(Player* onePlayer);
			//void CheckWinners();
	};

}