#pragma once
#include <iostream>
#include <string>
#include "stdafx.h"
#include "Players.h"
#include "CardDeck.h"

Player::Player()
{

}

Player::Player(std::string name, std::function<int()> callbackGetMove, std::function<void(int, std::string, int)> callbackSendMsg)
{
	Name = name;
	PlayerType = 0;
	CallbackGetPlayerMove = callbackGetMove;
	CallbackSendMsg = callbackSendMsg;
	Hand = new Deck(true);
}

Player::Player(std::string name) : Player(name, nullptr, std::bind([](int, std::string, int) {}, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3))
{
}

void Player::SetID(int playerID)
{
	PlayerID = playerID;
}

void Player::AddCard(Card* oneCard)
{
	Hand->AddCard(oneCard);
}

void Player::ShowHand()
{
	for (unsigned int i = 0; i < GetHand()->size(); i++)
	{
		printf("%s \n", GetHand()->at(i)->GetCardName().c_str());
	}
}

std::string Player::TakeCardFromDeck(Deck* GameDeck)
{
	if (GameDeck->GetCardsAmount() == 0)
	{
		GameDeck->AddNewCardDeck();
		GameDeck->ShufflingCards();
		printf("Add new CardDeck");
	}
	Card* PoppedCard = GameDeck->PopLastCard();
	AddCard(PoppedCard);
	return PoppedCard->GetCardName();
}

int Player::MakeMove() //gracz tylko podaje numer operacji gra weryfikuje czy dana operacja jest mo�liwa i wykonuje j�.
{
	return CallbackGetPlayerMove();
}

std::vector<Card*>* Player::GetHand()
{
	return Hand->GetDeck();
}

void Player::CleanHand()
{
	for (int i=0;Hand->GetDeck()->size();i++) Hand->RemoveLastCard();
}

int Player::AddMoneyOnTable(int amount)
{
	 if (Money - amount < 0)
		 return 0;

	 MoneyOnTable += amount;
	 Money -= amount;
	 return 1;
}

void Player::ResetMoneyOnTable()
{
	MoneyOnTable = 0;
}

void Player::GiveMoney(int amount)
{
	Money += amount;
}

int Player::GetMoney()
{
	return Money;
}
int Player::GetMoneyOnTable()
{
	return MoneyOnTable;
}

void Player::SetName(std::string name)
{
	Name = name;
}

std::string Player::GetName()
{
	return Name;
}

void Player::SetReady(bool ready)
{
	Ready = ready;
}

bool Player::IsReady()
{
	return Ready;
}
