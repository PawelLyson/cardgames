// Server.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <random>
#include "CardDeck.h"
#include "Players.h"
#include "Games.h"
#include "BlackJack.h"
#include "Globals.h"

extern Games GameServer;

int main()
{
	GameServer.StartGames();

	getchar();
    return 0;
}



