#pragma once
#include "stdafx.h"
#include "BlackJack.h"
#include "Players.h"
#include "CardDeck.h"
#include <iostream>
#include <string>
#include <vector>
#include "SimpleGame.h"

//GLOBAL
namespace bjGame 
{
	int ComputePointsInHand(Player* onePlayer)
	{
		int Points = 0;
		int PointsCard = 0;
		int NumberOfAs = 0;
		Card* OneCard;
		for (unsigned int i = 0; i < onePlayer->GetHand()->size(); i++)
		{
			OneCard = onePlayer->GetHand()->at(i);
			PointsCard = OneCard->GetValue() + 1;
			if (PointsCard > 10) PointsCard = 10;
			else if (PointsCard == 1)
			{
				PointsCard = 11;
				NumberOfAs++;
			}
			Points += PointsCard;
		}
		while (NumberOfAs && Points > 21)
		{
			NumberOfAs--;
			Points -= 10;
		}

		return Points;
	}

	//class BlackJack
	BlackJack::BlackJack()
	{
	}

	BlackJack::BlackJack(std::vector<Player*>* playersList) : SimpleGame(playersList)
	{
		GameCroupier = new Croupier();
		PlayersList->push_back(GameCroupier);
		PrepareDeck();
	}

	void BlackJack::PrepareGame()
	{
		SendMsgToAll(120, std::string("GetCard 2"), 0);
		GiveEveryoneCards(2);
		for (unsigned int i = 0; i < PlayersList->size(); i++)
		{
			PlayersList->at(i)->AddMoneyOnTable(50);
		}
		for (unsigned int i = 0; i < PlayersList->size(); i++)
		{
			SendMsgToAll(170, 
				std::to_string(PlayersList->at(i)->PlayerID) + std::string("|") + PlayersList->at(i)->GetName() + std::string("|") + std::to_string(PlayersList->at(i)->GetMoney()),
				PlayersList->at(i)->PlayerID);
		}
	}

	bool BlackJack::PlayerAction(Player* onePlayer, int move)
	{
		if (move == 1)
		{
			//printf("Wait\n");
			SendMsgToAll(137, std::string("Wait"), onePlayer->PlayerID);
			return false;
		}
		else if (move == 2)
		{
			SendMsgToAll(135, std::string("Hit"), onePlayer->PlayerID);
			return PlayerTakeCard(onePlayer);
		}
		else if (move == 3)
		{
			SendMsgToAll(136, std::string("Double down"), onePlayer->PlayerID);
			onePlayer->AddMoneyOnTable(onePlayer->GetMoneyOnTable());
			return PlayerTakeCard(onePlayer);
		}
		return false;
	}

	bool BlackJack::PlayerTakeCard(Player* onePlayer)
	{
		std::string CardName = onePlayer->TakeCardFromDeck(GameDeck);
		onePlayer->CallbackSendMsg(130, CardName, onePlayer->PlayerID);
		return true;
	}

	void BlackJack::CheckWinner()
	{
		int PlayerPoints = 0;
		int ComputerPoints = ComputePointsInHand(GameCroupier);
		SendMsgToAll(131, std::string("Kroupier Punkty ")+ std::to_string(ComputePointsInHand(PlayersList->at(PlayersList->size() - 1))), 0);
		//printf("Krupier:\nPunkty: %d\n\n", ComputePointsInHand(PlayersList->at(PlayersList->size() - 1)));
		for (unsigned int i = 0; i < PlayersList->size() - 1; i++)
		{
			//printf("%s\n\n", PlayersList->at(i)->Name.c_str());
			//printf("Punkty %d\n", ComputePointsInHand(PlayersList->at(i)));
			PlayerPoints = ComputePointsInHand(PlayersList->at(i));
			
			if (PlayerPoints >= 21 || (ComputerPoints <= 21 && ComputerPoints > PlayerPoints))
			{
				//printf("---Przegrana gracza");
				SendMsgToAll(151, "Lose", PlayersList->at(i)->PlayerID);
			}
			else if (PlayerPoints == ComputerPoints)
			{
				SendMsgToAll(152, "Draw", PlayersList->at(i)->PlayerID);
			}
			else
			{
				//printf("---Wygrana gracza");
				SendMsgToAll(153, "Win", PlayersList->at(i)->PlayerID);
			}
			PlayersList->at(i)->CleanHand();
			SendMsgToAll(155, std::string(""), PlayersList->at(i)->PlayerID);
		}
		IsOngoingRound = false;
	}

	int BlackJack::ComputePointsInHand(Player* onePlayer)
	{
		int Points = 0;
		int PointsCard = 0;
		int NumberOfAs = 0;
		Card* OneCard;
		for (unsigned int i = 0; i < onePlayer->GetHand()->size(); i++)
		{
			OneCard = onePlayer->GetHand()->at(i);
			PointsCard = OneCard->GetValue() + 1;
			if (PointsCard > 10) PointsCard = 10;
			else if (PointsCard == 1)
			{
				PointsCard = 11;
				NumberOfAs++;
			}
			Points += PointsCard;
		}
		while (NumberOfAs && Points > 21)
		{
			NumberOfAs--;
			Points -= 10;
		}

		return Points;
	}


	ComputerPlayer::ComputerPlayer(std::string name) : Player(name)
	{
		PlayerType = 1;
	}

	int ComputerPlayer::MakeMove()
	{
		//printf("\nImie: %s \n", Name.c_str());
		ShowHand();
		if (17 > ComputePointsInHand(this)) return 2;

		return 1;
	}

	std::string ComputerPlayer::GetName()
	{
		return Player::GetName();
	}

	void ComputerPlayer::SetReady(bool ready)
	{

	}
	bool ComputerPlayer::IsReady()
	{
		return true;
	}

	Croupier::Croupier() :ComputerPlayer(std::string("Croupier"))
	{
		PlayerType = 2;
		PlayerID = -1;
	}

	int Croupier::MakeMove()
	{
		//printf("\nCroupier:\n");
		if (17 > ComputePointsInHand(this)) return 2;
		return 1;
	}
	std::string Croupier::GetName()
	{
		if (Hand->GetCardsAmount() == 0) return "Croupier";
		else return Hand->GetCardName(0);
	}
}