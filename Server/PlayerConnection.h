#pragma once
#include <vector>
#include <string>
#include <thread>
#include <SFML\Network.hpp>
#include "Globals.h"
#include "Players.h"
#include "Games.h"

class PlayerConnection
{
private:
	//sf::Thread WaitForCommandThread;
	int PlayerStatus = 0;
	bool IsWaitingForCommand = true;
	int WaitTimeSec = 60;
	std::string PlayerName = "Bezimienny";
	int GameMove = 0;
	int IDConnectedLobby = 0;
	std::thread* WaitForCommandThread;
	sf::TcpSocket PlayerClient;
	void RerouteCommands(int type, std::string data);
	void ReceiveMsg();
public:
	int ClientID = 0;
	int WaitForPlayerCommand();
	Player ThisPlayer;
	PlayerConnection();
	PlayerConnection(sf::TcpListener* listenerTCP);
	void SendMsg(int type, std::string message);
	void SendFullMsg(int type, std::string message, int clientID);
	void SetInGame(bool set);
};

