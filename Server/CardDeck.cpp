#pragma once
#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include "CardDeck.h"

//Class card
//public:
Card::Card()
{

}
Card::Card(int type, int value)
{
	ActualType = type;
	ActualValue = value;
}
Card::~Card()
{
	//printf("delete2");
}

std::string Card::GetValueSymbolString()
{
	if (ActualType == 0)
		return "Heart";
	else if (ActualType == 1)
		return "Diamonds";
	else if (ActualType == 2)
		return "Clubs";
	else if (ActualType == 3)
		return "Spades";
	return "---";
}

std::string Card::GetTypeSymbolString()
{
	if (ActualValue == 0)
		return "AS";
	else if (ActualValue == 1)
		return "Two";
	else if (ActualValue == 2)
		return "Tree";
	else if (ActualValue == 3)
		return "Four";
	else if (ActualValue == 4)
		return "Five";
	else if (ActualValue == 5)
		return "Six";
	else if (ActualValue == 6)
		return "Seven";
	else if (ActualValue == 7)
		return "Eight";
	else if (ActualValue == 8)
		return "Nine";
	else if (ActualValue == 9)
		return "Ten";
	else if (ActualValue == 10)
		return "Jack";
	else if (ActualValue == 11)
		return "Queen";
	else if (ActualValue == 12)
		return "King";
	return "---";
}


int Card::GetValue()
{
	return ActualValue;
}
int Card::GetType()
{
	return ActualType;
}

std::string Card::GetCardName()
{
	return GetTypeSymbolString() + " " + GetValueSymbolString();
}

void Card::SetPoints(int value)
{
	CardPoints = value;
}

int Card::GetPoints()
{
	return CardPoints;
}


//private:

//class CardDeck
//public:

Deck::Deck(bool empty)
{
	CardDeck = new std::vector<Card*>();
	CardDeck->reserve(52);
	if (!empty)
	{
		AddNewCardDeck();
	}
}

void Deck::AddNewCardDeck()
{
	//int i = 0;
	for (int type = 0; type < 4; type++)
	{
		for (int value = 0; value < 13; value++)
		{
			CardDeck->push_back(new Card(type, value));
			//i++;
		}
	}
}

std::vector<Card*>* Deck::GetDeck()
{
	return CardDeck;
}

void Deck::AddCard(Card* OneCard)
{
	CardDeck->push_back(OneCard);
}

Card* Deck::PopCard(int CardNumber)
{
	Card* OneCard = CardDeck->at(CardNumber);
	CardDeck->erase(CardDeck->begin()+CardNumber);
	
	return OneCard;
}
Card* Deck::PopLastCard()
{
	Card* OneCard = CardDeck->back();
	CardDeck->pop_back();
	return OneCard;
}
std::string Deck::GetCardName(int number)
{
	return CardDeck->at(number)->GetCardName();
}
void Deck::RemoveLastCard()
{
	Card* OneCard = CardDeck->back();
	CardDeck->pop_back();
	delete OneCard;
}

void Deck::ShufflingCards()
{
	if (CardDeck->size() > 2)
	{
		gen = std::mt19937(rd());
		dis = std::uniform_int_distribution<>(0, 51);
		for (int i = 0; i < 2000; i++)
		{
			std::swap(CardDeck->at(dis(gen)), CardDeck->at(dis(gen)));
		}
	}
}

int Deck::GetCardsAmount()
{
	return CardDeck->size();
}

//private: