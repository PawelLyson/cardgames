#pragma once
#include <vector>
#include <string>
#include <thread>
#include <SFML\Network.hpp>
#include "Players.h"
#include "Globals.h"
#include "PlayerConnection.h"
#include "BlackJack.h"

class GameLobby
{
private:
	int GameID = 0;
	int MaxPlayers = 3;
	std::vector<PlayerConnection*> ConnectionsList;
	std::vector<Player*>* PlayerList;
	std::string GameName = "DefaultName";
	bjGame::BlackJack GameOne;
public:
	GameLobby();
	GameLobby(std::string name);
	int GetGameID();
	void AddPlayer(PlayerConnection* onePlayer);
	bool RemovePlayer(int playerID);
	void StartGame();
	void StopGame();
	void SetGameName(std::string gameName);
	int GetConnectedPlayersAmount();
	int GetMaxPlayersAmount();
	std::string GetGameName();
	void StartCardGameThread(std::vector<Player*>* playerList);
	std::thread* GameThread;
	bool IsEverybodyReady();
};

class Games
{
	private:
		sf::TcpListener ListenerTCP;
		std::vector<GameLobby> GameList;
		std::vector<PlayerConnection*> ConnectionList;
		void WaitForNewConnection();
	public:
		int LastGameID = 0;
		Games();
		~Games();
		void StartGames();
		std::string GetLobbyList();
		int CreateLobby(std::string name);
		void ConnectToGameLobby(int gameLobbyID, PlayerConnection& humanPlayer);
		void StartGame(int gameLobbyID);
		void LeaveGame(int clientID);
		void RemoveGame(int iterator);
		GameLobby* GetLobby(int gameLobbyID);
};