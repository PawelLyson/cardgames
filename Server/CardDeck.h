#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <random>


class Card
{
	private:
		int ActualType = 0;
		int ActualValue = 0;
		int CardPoints = 0;

	public:
		Card(int type, int value);
		Card();
		~Card();
		std::string GetValueSymbolString();
		std::string GetTypeSymbolString();
		std::string GetCardName();
		void SetPoints(int value);
		int  GetPoints();
		int GetType();
		int GetValue();
};

class Deck
{
	private:
		std::random_device rd;
		std::mt19937 gen;
		std::uniform_int_distribution<> dis;
		std::vector<Card*>* CardDeck;
		//card* CardDeck;

	public:
		Deck(bool empty);
		std::vector<Card*>* GetDeck();
		void AddCard(Card* OneCard);
		Card* PopCard(int CardNumber);
		Card* PopLastCard();
		std::string GetCardName(int number);
		void RemoveLastCard();
		void ShufflingCards();
		void AddNewCardDeck();
		int GetCardsAmount();
};