#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <SFML/Network.hpp>
#include <functional>
#include <stdlib.h>
#include "Globals.h"
#include "BlackJack.h"
#include "PlayerConnection.h"
#include "Games.h"
#include <ctime>

// do przemy�lenia
#include <windows.h>

extern Games GameServer;

struct DataPacketTCP
{
	sf::Int16 ClientID;
	sf::Int16 OperationID;
	std::string Data;
};

sf::Packet& operator <<(sf::Packet& packet, const DataPacketTCP& m)
{
	return packet << m.ClientID << m.OperationID << m.Data;
}
sf::Packet& operator >>(sf::Packet& packet, DataPacketTCP& m)
{
	return packet >> m.ClientID >> m.OperationID >> m.Data;
}


PlayerConnection::PlayerConnection()
{
}

PlayerConnection::PlayerConnection(sf::TcpListener* listenerTCP)
{
	//std::function<int(int)> CallbackGetPlayerMove = std::bind(PlayerConnection::WaitForPlayerCommand, this, _1);
	using namespace std::placeholders;
	std::function<int()> CallbackGetPlayerMove = std::bind(&PlayerConnection::WaitForPlayerCommand, this);
	std::function<void(int, std::string, int)> CallbackSendMsg = std::bind(&PlayerConnection::SendFullMsg, this, _1, _2, _3);
	ThisPlayer = Player(std::string("Name"), CallbackGetPlayerMove, CallbackSendMsg);

	//printf("Wait for clients\n");
	//sf::Thread WaitForCommandThread = sf::Thread(&PlayerConnection::isWaitingForCommand,this);
	if (listenerTCP->accept(PlayerClient) == sf::Socket::Done)
	{
		WaitForCommandThread = new std::thread(&PlayerConnection::ReceiveMsg, this);
		//printf("Connected\n");
	}
	else
	{
		//error
		printf("Error wait\n");
	}
}


int PlayerConnection::WaitForPlayerCommand()
{
	//printf("Waiting for Player Action %d\n", requestID);
	int PassedTime = 0; // 0.1 sec
	GameMove = 0;
	//SendMsg(100, "Your Turn\n");
	while (GameMove == 0 )//&& (WaitTimeSec * 1000 > PassedTime * 100))// nie mo�na pezerwa� cin u klienta
	{
		Sleep(100);
		PassedTime++;
	}
	//printf("Move was made\n");
	return GameMove;
}

void PlayerConnection::RerouteCommands(int type, std::string data)
{
	if (PlayerStatus == 3)
	{
		if (type == 100)
		{
			//printf("Player in Game");
		}
		else if (type == 101)
		{
			GameMove = std::stoi(data);
		}
		else if (type == 105)
		{
			ThisPlayer.SetReady(true);
		}
		else if (type == 106)
		{
			GameServer.LeaveGame(ClientID);
			PlayerStatus = 1;
			SendMsg(10000, "1");
		}
	}
	else if (PlayerStatus == 2)
	{
		if (type == 20)
		{
			//get Info
			GameLobby* ConnectedLobby = GameServer.GetLobby(IDConnectedLobby);
			SendMsg(20,
				(std::string)"Graczy podlaczaonych: " + std::to_string(ConnectedLobby->GetConnectedPlayersAmount()) + (std::string)"/" + std::to_string(ConnectedLobby->GetMaxPlayersAmount()) + (std::string)"\n"
				+ (std::string)"Nazwa: " + ConnectedLobby->GetGameName() + (std::string)"\n"
				+ (std::string)"Gra: BlackJack"
			);
		}
		else if (type == 21)
		{
			//setGame
		}
		if (type == 22)
		{
			PlayerStatus = 3;
			ThisPlayer.SetReady(true);
			GameServer.StartGame(IDConnectedLobby);
		}
	}
	else if (PlayerStatus == 1)
	{
		if (type == 2)
		{
			//GetLobbyList
			SendMsg(2, GameServer.GetLobbyList());
		}
		else if (type == 3)
		{
			PlayerStatus = 2;
			//ConnectGameLobby
			IDConnectedLobby = std::stoi(data);
			GameServer.ConnectToGameLobby(IDConnectedLobby, *this);
			SendMsg(10000, "2");
			PlayerStatus = 2;
		}
		else if (type == 4)
		{
			PlayerStatus = 2;
			//CreateLobby
			int CreatedLobbyID = GameServer.CreateLobby(data);
			IDConnectedLobby = CreatedLobbyID;
			GameServer.ConnectToGameLobby(IDConnectedLobby, *this);
			SendMsg(10000, "2");
			PlayerStatus = 2;
		}
		else if (type == 10)
		{
			PlayerName = data;
			ThisPlayer.SetName(data);
		}
		else
			SendMsg(2, "Returned = " + data);

	}
	else if (PlayerStatus == 0)
	{
		if (type == 1) //Rejestrowanie gracza
		{
			ClientID = std::stoi(data);
			ThisPlayer.SetID(ClientID);
			PlayerStatus = 1;
			SendMsg(10000, "1");
			printf("Register new player : %d\n", ClientID);
		}
		else
		{
			SendMsg(-1, "No Registration");
		}
	}
}
void PlayerConnection::SendMsg(int type, std::string message)
{
	SendFullMsg(type, message, ClientID);
}


void PlayerConnection::SendFullMsg(int type, std::string message, int clientID)
{
	DataPacketTCP DataPacket;

	DataPacket.ClientID = clientID;
	DataPacket.OperationID = type;
	DataPacket.Data = message;
	sf::Packet ToSendPacket;

	ToSendPacket << DataPacket;
	PlayerClient.send(ToSendPacket);

	ToSendPacket.clear();
}

void PlayerConnection::ReceiveMsg()
{
	sf::Packet ReturnedPacket;
	DataPacketTCP DataPacket;
	// TCP socket:
	IsWaitingForCommand = true;
	while (IsWaitingForCommand)
	{
		int ResultCode = PlayerClient.receive(ReturnedPacket);
		if (ResultCode == sf::Socket::Done)
		{
			ReturnedPacket >> DataPacket;
			RerouteCommands(DataPacket.OperationID, DataPacket.Data);
			//printf("Client: %d\n Operation: %d\nData: %s\n", DataPacket.ClientID, DataPacket.OperationID, DataPacket.Data.c_str());
			ReturnedPacket.clear();
		}
		else
		{
			printf("Data received error\n");
			break;
		}
	}

}

void PlayerConnection::SetInGame(bool set)
{
	if (set)
		PlayerStatus = 3;
	else
		PlayerStatus = 2;
}