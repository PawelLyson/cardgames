#include "stdafx.h"
#include <stdlib.h>
#include <SFML/Network.hpp>
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include "Games.h"
#include "Globals.h"
#include "BlackJack.h"
#include "PlayerConnection.h"

extern Games GameServer;

GameLobby::GameLobby()
{
	GameID = ++(GameServer.LastGameID);
}

GameLobby::GameLobby(std::string name) : GameLobby()
{
	GameName = name;
}

int GameLobby::GetGameID()
{
	return GameID;
}

void GameLobby::AddPlayer(PlayerConnection* onePlayer)
{
	ConnectionsList.push_back(onePlayer);
}

bool GameLobby::RemovePlayer(int playerID)
{
	for (unsigned int i = 0; i < ConnectionsList.size(); i++)
	{
		if (ConnectionsList.at(i)->ClientID == playerID)
		{
			ConnectionsList.erase(ConnectionsList.begin() + i);
			for (unsigned int i = 0; i < PlayerList->size(); i++)
			{
				if (PlayerList->at(i)->PlayerID == playerID)
				{
					PlayerList->erase(PlayerList->begin() + i);
					return true;
				}
			}
		}
	}
	return false;
}

void GameLobby::StartGame()
{
	PlayerList = new std::vector<Player*>();
	for (unsigned int i = 0; i < ConnectionsList.size(); i++)
	{
		PlayerList->push_back(&(ConnectionsList.at(i)->ThisPlayer));
		ConnectionsList.at(i)->SendMsg(10000, "3");
	}
	GameThread = new std::thread(&GameLobby::StartCardGameThread, this, PlayerList);
}

void GameLobby::StartCardGameThread(std::vector<Player*>* playerList)
{
	GameOne = bjGame::BlackJack(playerList);
	//printf("Starting Game\n");
	//GameOne.SendMsgToAll(125, std::string(""), 0);
	GameOne.RunGame();
}

void GameLobby::StopGame()
{
	printf("Stopping Game %s\n", GameName.c_str());
	GameOne.CloseGame();
	GameThread->join();
	printf("Stopped\n");
}

void GameLobby::SetGameName(std::string gameName)
{
	GameName = gameName;
}

int GameLobby::GetConnectedPlayersAmount()
{
	return (int)ConnectionsList.size();
}

int GameLobby::GetMaxPlayersAmount()
{
	return MaxPlayers;
}

std::string GameLobby::GetGameName()
{
	return GameName;
}

bool GameLobby::IsEverybodyReady()
{
	bool ready = true;
	for (unsigned int i = 0; i < ConnectionsList.size(); i++)
	{
		if (!ConnectionsList.at(i)->ThisPlayer.IsReady()) return false;
	}
	return ready;
}

Games::Games()
{

}

void Games::StartGames()
{
	WaitForNewConnection();
}


void Games::WaitForNewConnection()
{
	printf("Start listening\n");
	if (ListenerTCP.listen(53000) == sf::Socket::Done)
	{
		while (true)
		{
			ConnectionList.push_back(new PlayerConnection(&ListenerTCP));
		}
	}
	else
	{
		printf("Listening Error\n");
		//ERROR
	}
}

std::string Games::GetLobbyList()
{
	std::string Result = "";
	for (unsigned int i = 0; GameList.size() > i; i++)
	{
		Result += std::to_string(GameList.at(i).GetGameID()) + (std::string)": " + GameList.at(i).GetGameName() + (std::string)" Players: " + std::to_string(GameList.at(i).GetConnectedPlayersAmount()) + (std::string)"\n";
	}
	return Result;
}

int Games::CreateLobby(std::string name)
{
	GameList.push_back(GameLobby(name));
	return LastGameID;
}

Games::~Games()
{
}


void Games::ConnectToGameLobby(int gameLobbyID, PlayerConnection& humanPlayer)
{
	for (unsigned int i = 0; GameList.size() > i; i++)
	{
		if (GameList.at(i).GetGameID() == gameLobbyID)
		{
			printf("Added player to game\n");
			GameList.at(i).AddPlayer(&humanPlayer);
			return;
		}
	}
}

void Games::StartGame(int gameLobbyID)
{
	for (unsigned int i = 0; GameList.size() > i; i++)
	{
		if (GameList.at(i).GetGameID() == gameLobbyID)
		{
			if (GameList.at(i).IsEverybodyReady())
			{
				GameList.at(i).StartGame();
				printf("Started Game: %s\n", GameList.at(i).GetGameName().c_str());
			}
			//else printf("Wait for everybody");
			return;
		}
	}
}

void Games::LeaveGame(int clientID)
{
	unsigned int i = 0;
	for (i = 0; GameList.size() > i; i++)
	{
		if (GameList.at(i).RemovePlayer(clientID)) break;
	}
	if (!GameList.at(i).GetConnectedPlayersAmount()) RemoveGame(i);
}

void Games::RemoveGame(int iterator)
{
	GameList.at(iterator).StopGame();
	GameList.erase(GameList.begin() + iterator);
}

GameLobby* Games::GetLobby(int gameLobbyID)
{
	for (unsigned int i = 0; GameList.size() > i; i++)
	{
		if (GameList.at(i).GetGameID() == gameLobbyID)
		{
			return &GameList.at(i);
		}
	}
	return nullptr;
}

