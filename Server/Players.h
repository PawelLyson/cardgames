#pragma once
#include <string>
#include <iostream>
#include "CardDeck.h"
#include <functional>

class Player
{
private:
	std::string Name;
	int MoneyOnTable = 0;
	int Money = 500;
	bool Ready = false;

protected:
	Deck * Hand;

public:
	int PlayerType = 0;
	int PlayerID = 0;

	Player();
	Player(std::string name, std::function<int()> callbackMethod, std::function<void(int, std::string, int)> sendMsg);
	Player(std::string name);
				
	void AddCard(Card* OneCard);
	std::string TakeCardFromDeck(Deck* GameDeck);
	void ShowHand();
	void CleanHand();
	void ResetMoneyOnTable();
	void GiveMoney(int amount);
	int GetMoney();
	int GetMoneyOnTable();

	std::function<void(int, std::string, int)> CallbackSendMsg;
	std::function<int()> CallbackGetPlayerMove;

	int AddMoneyOnTable(int amount);
	std::vector<Card*>* GetHand();

	virtual int MakeMove();

	void SetName(std::string name);
	void SetID(int playerID);
	virtual std::string GetName();
	virtual void SetReady(bool ready);
	virtual bool IsReady();

};