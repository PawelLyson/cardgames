#pragma once
#include <iostream>
#include <vector>
#include "CardDeck.h"
#include "Players.h"

class SimpleGame
{
	protected:
		bool IsOngoingRound = true;
		bool IsGameRunning = true;
		int EverybodyWait = true;
	public:
		Deck* GameDeck;
		std::vector<Player*>* PlayersList;


		SimpleGame();
		SimpleGame(std::vector<Player*>* playersList);
		void RunGame();
		void CloseGame();
		void GiveEveryoneCards(int amount);
		void CleanAllHand();

		virtual void PrepareDeck();
		virtual void PrepareGame();
		virtual int ComputerMove(Player* computerPlayer);
		virtual int HumanMove(Player* humanPlayer);
		virtual bool PlayerAction(Player* onePlayer, int move);
		virtual void CheckWinner();
		virtual void EndGame();
		void SendMsgToAll(int cmdType, std::string data, int mainTarget); 
		int SendMsgToAllAndReciveOne(int cmdType, std::string data, int mainTarget);
		bool IsEverybodyReady();
		void ResetEverybodyReady();
		void SetGameRunning(bool set);

};

