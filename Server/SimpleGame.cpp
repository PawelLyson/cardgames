#pragma once
#include "stdafx.h"
#include "SimpleGame.h"
#include "CardDeck.h"
#include "Players.h"
#include <iostream>
#include <string>
#include <vector>
#include <windows.h>

SimpleGame::SimpleGame()
{

}


SimpleGame::SimpleGame(std::vector<Player*>* playersList)
{
	PlayersList = playersList;
}

void SimpleGame::PrepareDeck()
{
	GameDeck = new Deck(false);
	GameDeck->ShufflingCards();
}

void SimpleGame::PrepareGame()
{

}

void SimpleGame::RunGame()
{
	int PlayerMove = 0;
	while (IsGameRunning && PlayersList->size() > 0)
	{
		PrepareGame();
		IsOngoingRound = true;
		while (IsOngoingRound)
		{
			EverybodyWait = true;
			for (unsigned int i = 0; i < PlayersList->size(); i++)
			{
				if (PlayersList->at(i)->PlayerType == 0) PlayerMove = HumanMove(PlayersList->at(i));
				else PlayerMove = ComputerMove(PlayersList->at(i));

				if (PlayerAction(PlayersList->at(i), PlayerMove))
				{
					EverybodyWait = false;
				}
			}

			if (EverybodyWait)
			{
				CheckWinner();
			}
		}
		EndGame();
	}
}


void SimpleGame::CloseGame()
{
	SetGameRunning(false);
}

void SimpleGame::GiveEveryoneCards(int amount)
{
	for (int i = 0; i < amount; i++)
	{
		for (unsigned int i = 0; i < PlayersList->size(); i++)
		{
			std::string CardName = PlayersList->at(i)->TakeCardFromDeck(GameDeck);
			PlayersList->at(i)->CallbackSendMsg(130, CardName, PlayersList->at(i)->PlayerID);
		}
	}
}
void SimpleGame::CleanAllHand()
{
	for (unsigned int i = 0; i < PlayersList->size(); i++)
	{
		PlayersList->at(i)->CleanHand();
	}
}

bool SimpleGame::PlayerAction(Player* onePlayer, int move)
{
	return true;
}

void SimpleGame::CheckWinner()
{

}

int SimpleGame::HumanMove(Player* humanPlayer)
{
	//return humanPlayer->MakeMove();
	return SendMsgToAllAndReciveOne(100, std::string(""), humanPlayer->PlayerID);
}

int SimpleGame::ComputerMove(Player* computerPlayer)
{
	return computerPlayer->MakeMove();
}

void SimpleGame::EndGame()
{
	ResetEverybodyReady();
	//printf("Wait for everybody\n");
	SendMsgToAll(105, std::string(""), 0);
	while (!IsEverybodyReady())
	{
		Sleep(100);
	}
	CleanAllHand();
}

void SimpleGame::SendMsgToAll(int cmdType, std::string data, int mainTarget)
{
	for (unsigned int i = 0; i < PlayersList->size(); i++)
	{
		PlayersList->at(i)->CallbackSendMsg(cmdType, data, mainTarget);
	}
}

int SimpleGame::SendMsgToAllAndReciveOne(int cmdType, std::string data, int mainTarget)
{
	int ReturnValue = 0;
	for (unsigned int i = 0; i < PlayersList->size(); i++)
	{
		PlayersList->at(i)->CallbackSendMsg(cmdType, data, mainTarget);
	}
	for (unsigned int i = 0; i < PlayersList->size()-1; i++) //bez krupiera
	{
		if (PlayersList->at(i)->PlayerID == mainTarget)
		{
			return PlayersList->at(i)->CallbackGetPlayerMove();
		}
	}
	return ReturnValue;
}

bool SimpleGame::IsEverybodyReady()
{
	bool ready = true;
	for (unsigned int i = 0; i < PlayersList->size(); i++)
	{
		if (!PlayersList->at(i)->IsReady()) return false;
	}
	return ready;
}

void SimpleGame::ResetEverybodyReady()
{
	for (unsigned int i = 0; i < PlayersList->size(); i++)
	{
		PlayersList->at(i)->SetReady(false);
	}
}

void SimpleGame::SetGameRunning(bool set)
{
	IsGameRunning = set;
}